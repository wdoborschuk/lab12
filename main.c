#include <stdio.h>
#include "calculate.h"

int main (void) {
	float Numeral;
	char Operation[4];
	float Result;
	int flag = 1;
	while(flag) {
		printf("Число: ");
		scanf("%f", &Numeral);
		printf("Операция (+,-,*,/,pow,sqrt,sin,cos,tan): ");
		scanf("%s", &Operation);
		Result = Calculate(Numeral, Operation);
		printf("%6.2f\n", Result);
		printf("Для повтора введите: 1, для выхода: 0\n Ввод: ");
		scanf("%f", &flag);
	}
 
	return 0;
}
